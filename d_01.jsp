<!-예약상황->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>Reservation</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type="text/css">
body {margin:100; background-color:FDF5E6;}
table {align:center; width:800; cellpadding:10; background-color:ffffff;}
td {text-align:center; cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>

</head>
<body>
<%
//예약리스트 출력
int totaldata = 0;  // 전체 데이터 수
int countpage = 5;    //한화면에 나올 페이지번호 수 
int datalist = 10;   //출력될 데이터 수
int totalpage = 0;     //전체 페이지 수
int linecount = 0;
// 페이지 변경시 시작 번호를 변경하기 위한 변수 계산  
String starts = request.getParameter("togo"); 

int start;                                            

try{
	// getParameter는 문자열로 인자를 받기 때문에 int형으로 형변환이 필요하다
     start = Integer.parseInt(starts);        
} catch (Exception e){
	//try catch 구문을 활용하여 초기 변수값이 없어 발생하는 오류를 응용, start 에 초기 값을 부여하였다.
     start = 0;      //현재페이지                    
}
if(start <0){
       start = 0;
}  


request.setCharacterEncoding("UTF-8");
	
	String write_date = "";
	String suits = "";
	String standard = "";
	String business = "";
	String[][] resv_arr=new String[30][5];
	
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
	SimpleDateFormat df2 = new SimpleDateFormat("EEE",Locale.KOREA);

//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();

//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	ResultSet rset = stmt.executeQuery("select resv_date, max(case when room=1 then name end) as suits,"+ 
										"max(case when room=2 then name end) as standard, max(case when room=3 then name end)"+ 
										"as business from resort group by resv_date;");
	//sql문에 group by나 join을 사용해 중복 날짜를 합쳐서 표시해 본다..
	for (int i=0; i <30; i++){//배열 넣기
		resv_arr[i][0] = df.format(cal.getTime());
		resv_arr[i][1] = df2.format(cal.getTime());
		resv_arr[i][2] = "Available";
		resv_arr[i][3] = "Available";
		resv_arr[i][4] = "Available";
		cal.add(cal.DATE,+1);
	}	
		while(rset.next()){
			write_date = rset.getString(1);
			suits = rset.getString(2);
			standard = rset.getString(3);
			business = rset.getString(4);
			if(suits==null){suits ="Available";}
			if(standard==null){standard ="Available";}
			if(business==null){business ="Available";}
					// ~ 번부터 ~번 까지 출력
      if (linecount < start){         
          linecount++; 
          continue;
      }
			for(int i=0;i<resv_arr.length;i++){	//db의 이름을 배열에 넣어준다.
				if(resv_arr[i][0].equals(write_date)){
					resv_arr[i][2] = suits;
					resv_arr[i][3] = standard;
					resv_arr[i][4] = business;
	
				}
			}
		}
%>

<table align=center>
	<tr>
	<td width=200> Date </td>
	<td width=200> Suits </td>
	<td width=200>Standard </td>
	<td width=200>Business </td>
	</tr>
	
<%
for (int i=0; i<resv_arr.length;i++){
			out.println("<tr><td>"+resv_arr[i][0]+" ("+resv_arr[i][1]+")</td>");
			if(resv_arr[i][2].equals("Available")){
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=1'>"+resv_arr[i][2]+"</a></td>");
			}else{
			out.println("<td>"+resv_arr[i][2]+"</td>");
			}
			if(resv_arr[i][3].equals("Available")){
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=2'>"+resv_arr[i][3]+"</a></td>");	
			}else{
			out.println("<td>"+resv_arr[i][3]+"</td>");	
			}
			if(resv_arr[i][4].equals("Available")){
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=3'>"+resv_arr[i][4]+"</a></td></tr>");
			}else{
			out.println("<td>"+resv_arr[i][4]+"</td>");	
			}
//페이지 수 계산을 위한 데이터 값 적산
	linecount++;		
	if(linecount >= start+datalist){
           break;
     }			
	}

	
	

%>
</table>
<br>
<table align=center>
<tr><td width=520></td><td><input align=right type=submit onClick=location.href='d_02.jsp'
		value = Booking></input></td></tr>

</table>
<table align=center cellspacing=1 width=300 style="font-size:20;">
<tr>

<%
//페이지네이션
ResultSet rset2 = stmt2.executeQuery("select count(*) from resort;");
rset2.next();
   totaldata = rset2.getInt(1); // 전체 데이처 수 계산

	//총페이지 수 계산
   if(totaldata != 0){
      if((totaldata % datalist) == 0){
         totalpage = (totaldata/datalist);
      }else{
         totalpage = (totaldata/datalist) + 1;
      } 
   }      
   
   if(start >= (totalpage*datalist)-datalist){
      start = (totalpage*datalist)-datalist;
    }   // 맨마지막 페이지를 클릭 했을때 다음 페이지로 넘어가못하도록 출력되는 페이지의 한계숫자를 조정
	String first = "Top";
    String before = "Prev";    
    String nex = "Next";
    String over = "Bottom";
	//클릭한 번호에 따라 보이는 페이지 번호의 시작과 끝을 변경하는 식
     int startpage = (start/(countpage*datalist)) * countpage +1;  
     int endpage = startpage + countpage -1;
    // 맨마지막 페이지의 번호만 나오도록 endpage숫자가 총 페이지 수보다 클 경우 강제로 총페이지수를 입력
    if(endpage > totalpage) {
       endpage = totalpage;
    }    

//페이지 번호 출력 식  
    if(startpage >=1){
       out.println("<td width=80><a href = d_01.jsp?togo="+ 0 +">"+" "+ first +" "+"</a></td>");
    }
    //이전
    if(start>=0){
       out.println("<td width=80><a href=d_01.jsp?togo="+ (start -datalist) +">"+" "+ before +" "+"</a></td>");
    }
	//출력될 페이지 번호 계산
     for(int i = startpage; i <= endpage; i++){
       if (i == start){          
         out.println("<td width=200><a href = d_01.jsp?togo="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>");
       }        
       else {
       out.println("<td width=200><a href = d_01.jsp?togo="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>");   
    }    
    }

    //다음
    if (start <= totalpage*datalist) {    
		int next = start+datalist;
		if(next>=totaldata){
			next=(totalpage-1)*datalist;
	} 
		out.println("<td width=80><a href=d_01.jsp?togo="+ (next) +">"+" "+ nex +" "+"</a></td>");
    } 
	//끝
    if (endpage <= totalpage) {	
		totalpage = (totalpage-1)*datalist;
		out.print("<td width=80><a href=d_01.jsp?togo="+ totalpage +">"+" "+ over +" "+"</a></td>"); 
	} 			
	rset.close();
	stmt.close();
	stmt2.close();
	conn.close();

%>
</tr>
</table>


</body>
</html>