<!- id, pw 일치 여부로 로그인 확인 ->

<%@page import="java.sql.*, javax.sql.*, java.net.*, java.io.*"%>
<%@page contentType="text/html; charset=utf-8" language="java"%>
<html>
<head>
<!- css 사용 : img, body, table, td 출력 형식 지정 ->
<style type="text/css">
img { display: block; margin: 10px auto; }
body {margin:100; background-color:FDF5E6;}
table {width:600; cellpadding:10; background-color:ffffff;}
td {cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>

</head>
<body>
<center>
<%
	request.setCharacterEncoding("utf-8");
	
	String jump = request.getParameter("jump");
	String id = request.getParameter("id");
	String pass = request.getParameter("passwd");
	
	boolean bPassChk=false;
	
	//공백 제거하고 id, pw 확인
	if(id.replaceAll(" ","").equals("admin")&&pass.replaceAll(" ","").equals("admin"))
	{
		bPassChk=true;
	} else {
		bPassChk=false;
	}
	
	//세션 기록하고 점프
	if(bPassChk) {
		session.setAttribute("login_ok","yes");
		session.setAttribute("login_id", id);
		response.sendRedirect(jump);
	} else {
		out.println("<h2>아이디 또는 패스워드 오류.</h2>");
		out.println("<input type=button value='로그인' onClick=\"location.href='adm_allview.jsp?jump="+jump+"'\">");
	}
	
%>
</center>
</body>
</html>