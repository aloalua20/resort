<!- 예약 수정하기 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>Reservation</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type="text/css">
h2 {text-align:center;}
body {margin:100; background-color:FDF5E6;}
table {align:center; width:800; cellpadding:10; background-color:ffffff;}
td {text-align:center; cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>
<script type="text/javascript">
<!--뒤로가기 메서드-->
function goBack(){
window.history.back();
}
</script>
</head>
<body>
<%
//try{
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
//post 방식으로 파라메터 가져올 때 한글 사용 코드
	request.setCharacterEncoding("UTF-8");
	String resv_date = request.getParameter("resv_date");
	String room = request.getParameter("room");
	out.println(resv_date);
	out.println(room);

	 //select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	 //resv_date와 room에 일치하는 항목 조회
	 ResultSet rset = stmt.executeQuery("select * from resort where resv_date='"+resv_date+"' and room='"+room+"';");
	 rset.next();
	 String name = rset.getString(1); 
	 String addr = rset.getString(4);
	 String telnum = rset.getString(5);
	 String in_name = rset.getString(6);
	 String comment = rset.getString(7);	 
	 
	 //방의 코드에 따라 해당 문자열로 출력
	 String room2 = "";
	 if (room.equals("1")) {room2 = "Suits";}
	 else if (room.equals("2")) {room2 = "Standard";}
	 else if (room.equals("3")) {room2 = "Business";}
%>
<br>
<h2>예약 수정</h2>
<br>
<table align=center>
<form method=post action='adm_write.jsp?resv_date=<%=resv_date%>&room=<%=room%>'>
<tr>
<td width=200>성명</td>
<td width=400><input type=text name ='name' value='<%=name%>'></td>
</tr>
<tr>
<td width=200>예약일자</td>
<td width=400><input type=hidden name ='resv_date' ><%=resv_date%></td>
</tr>
<tr>
<td width=200>예약방</td>
<td width=400><input type=hidden name="room"><%=room2%></td>
</tr>
<tr>
<td width=200>주소</td>
<td width=400><input type=text name ='addr' value='<%=addr%>'></td>
</tr>
<tr>
<td width=200>전화번호</td>
<td width=400><input type=text name ='telnum' value='<%=telnum%>'></td>
</tr>
<tr>
<td width=200>입금자명</td>
<td width=400><input type=text name ='in_name' value='<%=in_name%>'></td>
</tr>
<tr>
<td width=200>남기실말</td>
<td width=400><textarea type=text style="resize:none" required name ='comment' cols=20 rows=5 value='<%=comment%>'><%=comment%></textarea></td>
</tr>
</table>
<br>
<table align=center>
	<tr>
	<td><input type=button onClick='goBack();' value = '취소'> </input></td>
	<td>
	<input type=submit value = '등록'></input></td>
</form>
	<td><input type=button onClick=location.href='adm_delete.jsp?resv_date=<%=resv_date%>&room=<%=room%>'  value = '삭제'></input></td>
	</tr>
</table>


<%
	
	conn.close();
	
// } catch(SQLException e) {
      // if(extractErrorCode(e)==1062){
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            // out.println("데이터가 중복됩니다.");
         // }else if(extractErrorCode(e)==1146){
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            // out.println("테이블이 존재하지 않습니다.");
         // }else{
            // out.println("[기타] <br>");
            // out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            // out.println("입력값이 없습니다.");
         // }
// }
	
%>

</body>
</html>