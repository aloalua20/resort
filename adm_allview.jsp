<!- 로그인/ 전체 예약 조회 ->

<!-hmtl에서 한글 설정, 브라우저에게 encoding형식 알림->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>Admin page</title>
<head>

<!- css 사용 : img, body, table, td 출력 형식 지정 ->
<style type="text/css">
img { display: block; margin: 10px auto; }
body {margin:100; background-color:FDF5E6;}
table {width:600; cellpadding:10; background-color:ffffff;}
td {cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>

</head>
<%
	String loginOK=null;
	String jumpURL="adm_login.jsp?jump=adm_allview.jsp";
	
	loginOK = (String)session.getAttribute("login_ok");
	if(loginOK==null) {
		response.sendRedirect(jumpURL);
		return;
	}
	if(!loginOK.equals("yes")) {
		response.sendRedirect(jumpURL);
		return;
	}
%>
</head>
<body>
<center>
<h1>로그인 오케이</h1>
<br><br>
<%

request.setCharacterEncoding("UTF-8");
	
	String write_date = "";
	String resv_date= "";
	String suits = "";
	String standard = "";
	String business = "";
	String[][] resv_arr=new String[30][5];
	
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
	SimpleDateFormat df2 = new SimpleDateFormat("EEE",Locale.KOREA);

//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();

//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	ResultSet rset = stmt.executeQuery("select resv_date, max(case when room=1 then name end) as suits, max(case when room=2 then name end)"+ 
										"as standard, max(case when room=3 then name end) as business from resort group by resv_date;");
	//sql문에 group by나 join을 사용해 중복 날짜를 합쳐서 표시해 본다..
	for (int i=0; i <30; i++){//배열 넣기
		resv_arr[i][0] = df.format(cal.getTime());
		resv_arr[i][1] = df2.format(cal.getTime());
		resv_arr[i][2] = "예약 가능";
		resv_arr[i][3] = "예약 가능";
		resv_arr[i][4] = "예약 가능";
		cal.add(cal.DATE,+1);
	}	
		while(rset.next()){
			resv_date = rset.getString(1);
			
			suits = rset.getString(2);
			standard = rset.getString(3);
			business = rset.getString(4);
			if(suits==null){suits ="예약 가능";}
			if(standard==null){standard ="예약 가능";}
			if(business==null){business ="예약 가능";}
			
      
			for(int i=0;i<resv_arr.length;i++){	//db의 이름을 배열에 넣어준다.
				if(resv_arr[i][0].equals(resv_date)){
					resv_arr[i][2] = suits;
					resv_arr[i][3] = standard;
					resv_arr[i][4] = business;
				}
			}
		}
%>

<table align=center>
	<tr>
	<td width=200> Date </td>
	<td width=200> Suits </td>
	<td width=200>Standard </td>
	<td width=200>Business </td>
	</tr>
	
<%
for (int i=0; i<resv_arr.length;i++){
			out.println("<tr><td>"+resv_arr[i][0]+" ("+resv_arr[i][1]+")</td>");
			if(resv_arr[i][2].equals("예약 가능")){
			out.println("<td><a href='d_02.jsp?admin=admin&resv_date="+resv_arr[i][0]+"&room=1'>"+resv_arr[i][2]+"</a></td>");
			}else{
			out.println("<td><a href=adm_oneview.jsp?resv_date="+resv_arr[i][0]+"&room=1>"+resv_arr[i][2]+"</a></td>");
			}
			if(resv_arr[i][3].equals("예약 가능")){
			out.println("<td><a href='d_02.jsp?admin=admin&resv_date="+resv_arr[i][0]+"&room=2'>"+resv_arr[i][3]+"</a></td>");	
			}else{
			out.println("<td><a href=adm_oneview.jsp?resv_date="+resv_arr[i][0]+"&room=2>"+resv_arr[i][3]+"</a></td>");	
			}
			if(resv_arr[i][4].equals("예약 가능")){
			out.println("<td><a href='d_02.jsp?admin=admin&resv_date="+resv_arr[i][0]+"&room=3'>"+resv_arr[i][4]+"</a></td></tr>");
			}else{
			out.println("<td><a href=adm_oneview.jsp?resv_date="+resv_arr[i][0]+"&room=3>"+resv_arr[i][4]+"</a></td>");	
			}
	
	}
	rset.close();
	stmt.close();
	stmt2.close();
	conn.close();
%>
</table>
<br>
</body>
</html>
<br>
<a href="adm_logout.jsp">로그아웃</a>
</center>
</body>
</html>