<!- 예약 수정하는 파일 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>Reservation</title>
<head>

<style type="text/css">
body {margin:100; background-color:FDF5E6;}
table {align:center; width:800; cellpadding:10; background-color:ffffff;}
td {text-align:center; cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>

</head>
<body>

<%
	request.setCharacterEncoding("UTF-8");
	//insert.jsp, reinsert.jsp파일에서 파라미터 받아오기
	String name = request.getParameter("name");
	String addr = request.getParameter("addr");
	String telnum = request.getParameter("telnum");
	String in_name = request.getParameter("in_name");
	String comment = request.getParameter("comment");
	String resv_date = request.getParameter("resv_date");
	String room = request.getParameter("room");

	//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
	
	//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");

		String sql = "update resort set name=?, addr=?, telnum=?, in_name=?, comment=? where resv_date = ? and room=?;";

		PreparedStatement pstmt = conn.prepareStatement(sql);
				
		pstmt.setString(1, name);
		pstmt.setString(2, addr); 
		pstmt.setString(3, telnum);
		pstmt.setString(4, in_name);
		pstmt.setString(5, comment);
		pstmt.setString(6, resv_date);
		pstmt.setString(7, room);
		
		pstmt.executeUpdate();
		pstmt.close();


%>

<%
	
	conn.close();
%>
<script type = 'text/javascript'> 
location.href="adm_oneview.jsp?resv_date=<%=resv_date%>&room=<%=room%>";
</script>
</body>
</html>