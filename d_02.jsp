<!-예약하기->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>예약하기</title>
<head>
<!- summernote css/js 주소 추가 ->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script>
$(document).ready(function() {
     $('#summernote').summernote({
             height: 500,                 // set editor height
             minHeight: null,             // set minimum height of editor
             maxHeight: null,             // set maximum height of editor
             focus: true                  // set focus to editable area after initializing summernote
     });
});
$(document).ready(function() {
  $('#summernote').summernote();
});
</script>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<SCRIPT LANGUAGE="JavaScript">
	var cont = '';
	function submitForm(mode){
		//title과 content의 작성글 검사. 기호나 공백을 막았다.
		var name = document.forms[0].name.value;
		var resv_date = document.forms[0].resv_date.value;
		var telnum = document.forms[0].telnum.value;
		var in_name = document.forms[0].in_name.value;
		if(name == null || name == ""){
			alert('이름을 입력하세요.');
			document.forms[0].name.focus();
			return false;
		}
		if(name.match(/[^가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z0-9\s]/gi)){
			alert('특수문자 사용이 불가능합니다.');
			document.forms[0].name.focus();
			return false;
		}
		if(resv_date == null || resv_date == ""){
			alert('날짜를 입력하세요.');
			document.forms[0].resv_date.focus();
			return false;
		}
		if(resv_date.match(/[^0-9\s\-]/gi)){
			alert('올바른 형식이 아닙니다.');
			document.forms[0].resv_date.focus();
			return false;
		}
		if(telnum == null || telnum == ""){
			alert('연락처를 입력하세요.');
			document.forms[0].telnum.focus();
			return false;
		}
		if(telnum.match(/[^0-9\-]/gi)){
			alert('올바른 형식이 아닙니다.');
			document.forms[0].telnum.focus();
			return false;
		}
		if(in_name == null || in_name == ""){
			alert('이름을 입력하세요.');
			document.forms[0].in_name.focus();
			return false;
		}
		if(in_name.match(/[^가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z0-9\s]/gi)){
			alert('특수문자 사용이 불가능합니다.');
			document.forms[0].in_name.focus();
			return false;
		}
		//특정 key를 입력하고 전송한다.
		fm.action = "d_02_write.jsp";
		fm.submit();
	}
</SCRIPT>
<style type="text/css">
img { display: block; margin: 10px auto; }
body {margin:100; background-color:FDF5E6;}
table {width:600; cellpadding:10px; background-color:ffffff; text-align:center;}
tr {cellpadding:50; text-align:center;}
td {cellpadding:50; text-align:center; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>
</head>

<%

	request.setCharacterEncoding("UTF-8");
	
 java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
 String today = formatter.format(new java.util.Date());
	
	String resv_date = request.getParameter("resv_date");
	if(resv_date==null){resv_date="'예약 현황에서 예약 날짜를 선택해 주세요' readonly";}
	String room = request.getParameter("room");
	if(room==null){room="0";}

 %>

<body>
<table align=center>
<!- 입력받은 값을 d_02_write.jsp로 보냄 ->
<form method=post action='d_02_write.jsp' name="fm">
<tr>
<td width=200>Name</td>
<td width=400><input type=text name ='name' required></td>
</tr>
<tr>
<td width=200>Date</td>
<td width=400><input type=text name ='resv_date' value=<%=resv_date%>></td>
</tr>
<tr>
<td width=200>Room</td>
<td width=400><select name="room">
<%if(room.equals("1")){
						out.println("<option value=1 selected>Suits</option>");
						out.println("<option value=2>Standard</option>");
						out.println("<option value=3>Business</option>");
				 }else if(room.equals("2")){
						out.println("<option value=1>Suits</option>");
						out.println("<option value=2 selected>Standard</option>");
						out.println("<option value=3>Business</option>");  
				 }else if(room.equals("3")){			
						out.println("<option value=1>Suits</option>");
						out.println("<option value=2>Standard</option>");
						out.println("<option value=3 selected>Business</option>");  
				 } else{
						out.println("<option disabled hidden selected>방 선택</option>");
						out.println("<option value=1>Suits</option>");
						out.println("<option value=2>Standard</option>");
						out.println("<option value=3>Business</option>");
				 }

 
%>				 
</select>
</td>
</tr>
<tr>
<td width=200>Address</td>
<td width=400><input type=text name ='addr' ></td>
</tr>
<tr>
<td width=200>Phone</td>
<td width=400><input type=text name ='telnum' ></td>
</tr>
<tr>
<td width=200>Name on Payment</td>
<td width=400><input type=text name ='in_name' ></td>
</tr>
<tr>
<td width=200>Note</td>
<td width=400><textarea id="summernote" type=text style="resize:none" required name ='comment' cols=20 rows=5></textarea></td>
</tr>
<tr>
<td><input type='hidden' name='write_date' value=<%=today%>></td>
<td><input type='hidden' name='processing' value='1'></td>
</table>
<br><br>
<table align=center>
<td><input type=submit value = '쓰기'></input></td>
</form>
<%
String admin=request.getParameter("admin");
String go ="";
if (admin!=null) {
	go ="adm_allview.jsp";
} else {
	go="d_01.jsp";
}
%>
<td><input type=button onClick=location.href='<%=go%>' value = '취소'> </input></td>
</tr>
</table>

</body>
</html>
