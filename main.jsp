<!- 리조트 웹사이트 첫페이지 ->

<!-hmtl에서 한글 설정, 브라우저에게 encoding형식 알림->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page language="java" import="java.text.DateFormat, java.util.Date" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*"  %>
<!- css사용을 위한 링크 ->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>The Resort</title>
<head>
<!- css로 img, body의 포멧 정의 ->
<style type="text/css">
img { 
	display: block; margin: 10px auto; 
	font-family: Georgia;
	}
body {
	margin:100; background-color:FDF5E6; 
	font-family: Georgia;
	}
strong { 
  font-weight: bold;
  font-size:30;
	}
</style>
<!- 팝오버 사용을 위한 자바스크립트 함수 ->
<script language='javascript' type='text/javascript'>
$(function () {
  $('[data-toggle="popover"]').popover()
})
$('.popover-dismiss').popover({
  trigger: 'focus'
})
</script>
</head>
<body>
<center>
<!- 방문자 쿠키 ->
<%
    //마지막 방문일을 저장하고 있는 쿠키를 저장할 객체
    Cookie lastDate = null;   
    //화면에 출력할 메시지를 저장할 문자열 변수
    String msg = "";    
    //마지막 방문일을 저장하고 있는 쿠키가 존재하는지를 판별할 변수
    boolean found = false; 
    //현재 시간을 저장
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
    String newValue = "" + formatter.format(new java.util.Date());   
    //쿠키를 얻는다.
    Cookie[] cookies = request.getCookies();   
    //마지막 방문 일을 정하고 있는 쿠키를 검색
    if (cookies != null) { //쿠키값이 null이 아닌 경우
        for (int i = 0; i < cookies.length; i++) {
            lastDate = cookies[i];
            if (lastDate.getName().equals("lastdateCookie")) {  
                found = true;
                break;
            }
        }
    }
	//마지막 방문시간 출력을 위한 변수 선언
	long conv = 0;
	String year = ""; //연도
	String month = ""; //월
	String day = ""; //일 
	String hour = ""; //시간
	String minute = ""; //분
    
    //처음 방문일 경우 새 쿠키 생성
    if (!found) {    // if (found = false)   
        msg = "First Visit......";

        //쿠키 객체를 생성
        lastDate = new Cookie("lastdateCookie", newValue);
        //쿠키 속성값을 설정
        lastDate.setMaxAge(365*24*60*60);    // 365일
        lastDate.setPath("/"); 
        //쿠키를 추가
        response.addCookie(lastDate);
    }
	
    // 이미 방문한 적이 있는 클라이언트라면
    else {     
        //이전 방문시간을 알아내어 long형 변수 conv에 저장
        conv = new Long(lastDate.getValue()).longValue();
        //방문시간을 출력할 수 있도록 msg 변수에 저장
        String strLong = Long.toString(conv);
        year = "Year"+strLong.substring(0,4);
        month = "Month"+strLong.substring(4,6);
        day = "Day"+strLong.substring(6,8);
        hour = "Hour"+strLong.substring(8,10);
        minute = "Minute"+strLong.substring(10,12);
        msg = "Your Last Visit : " + year + month + day + hour + minute;
        
        //쿠키에 새 값을 추가
        lastDate.setValue(newValue);   
        //쿠키를 추가
        response.addCookie(lastDate);
    }
	
%>
<!- 방문자에 대한 메세지 출력 ->
<h2><%=msg%></h2>
<br>
<!- carousel 슬라이드쇼 사용을 위한 div 블럭 ->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
     <img src="/file/hotel1.jpeg" alt="hotel1">
	 <div class="carousel-caption d-none d-md-block">
     <h3>A Touch of the Exotic</h3>
     <p>Find yourself here</p>
	 </div>
    </div>
    <div class="carousel-item">
     <img src="/file/hotel2.jpeg" alt="hotel2">
	 <div class="carousel-caption d-none d-md-block">
     <h3>Refined by Nature</h3>
     <p>Fresh and clean environment for your relaxation</p>
	 </div>
    </div>
    <div class="carousel-item">
     <img src="/file/hotel4.jpeg" alt="suits3">
     <div class="carousel-caption d-none d-md-block">
     <h3>For the Best Offer</h3>
     <p>Join Resort Membership and find out more values</p>
	 </div>
    </div>
  </div>
</div>
<br><br>
<!- 팝오버 사용 : 'Click here' 누르면 팝오버 내용 출력 ->
<div class="container">
	<button type="button" class="btn btn-default" title="The Resort Values" data-container="body" data-toggle="popover" data-placement="bottom" 
	data-content="We gladly offer various sorts of accommodations, activities and 
				exclusive reservation system. Reviews by guests are available on Comment menu 
				to ensure you our best service.">
		<strong>Click here to find out our values</strong>
	</button>
</div>
</body>
</html>