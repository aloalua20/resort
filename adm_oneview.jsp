<!- 예약 하나 보기 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<!-캐시삭제 (이하 세 줄)>
<meta http-equiv="Cache-Control" content="no-cache"/> 
<meta http-equiv="Expires" content="0"/> 	
<meta http-equiv="Pragma" content="no-cache"/>	

<html>
<title>Reservation</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type="text/css">
body {margin:100; background-color:FDF5E6;}
table {align:center; width:800; cellpadding:10; background-color:ffffff;}
td {text-align:center; cellpadding:50; border-top:1px solid #808000; border-bottom:1px solid #808000;}
</style>
<script type="text/javascript">
<!--뒤로가기 메서드-->
function goBack(){
window.history.back();
}
</script>
</head>
<body>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	  Statement stmt = conn.createStatement();
	  //get으로 받아 string이므로 String 변수명1 = request.getParameter(기존변수명); 으로 값 받아옴
	  //이전 페이지에서 key로 지정 된 파라메터를 받아 key문자열에 적용
	  
	  String resv_date = request.getParameter("resv_date");
	  String room = request.getParameter("room");

	 //resv_date와 room에 일치하는 항목 조회
	 ResultSet rset = stmt.executeQuery("select * from resort where resv_date='"+resv_date+"' and room='"+room+"';");
	 rset.next();
	 String name = rset.getString(1); 
	 String addr = rset.getString(4);
	 String telnum = rset.getString(5);
	 String in_name = rset.getString(6);
	 String comment = rset.getString(7);	 
	 
	 String room2 = "";
	 if (room.equals("1")) {room2 = "Suits";}
	 else if (room.equals("2")) {room2 = "Standard";}
	 else if (room.equals("3")) {room2 = "Business";}
%>

<table align=center>
<form method=post action='adm_update.jsp?resv_date=<%=resv_date%>&room=<%=room%>' >
<tr>
<td width=200>성명</td>
<td width=400><input type=hidden name ='name' value='<%=name%>'><%=name%></td>
</tr>
<tr>
<td width=200>예약일자</td>
<td width=400><input type=hidden name ='resv_date' value=<%=resv_date%>><%=resv_date%></td>
</tr>
<tr>
<td width=200>예약방</td>
<td width=400><input type=hidden name="room" value='<%=room%>'><%=room2%></td>
</tr>
<tr>
<td width=200>주소</td>
<td width=400><input type=hidden name ='addr' value='<%=addr%>'><%=addr%></td>
</tr>
<tr>
<td width=200>전화번호</td>
<td width=400><input type=hidden name ='telnum' value='<%=telnum%>'><%=telnum%></td>
</tr>
<tr>
<td width=200>입금자명</td>
<td width=400><input type=hidden name ='in_name' value='<%=in_name%>'><%=in_name%></td>
</tr>
<tr>
<td width=200>남기실말</td>
<td width=400><textarea class='noresize' name ='comment' value='<%=comment%>'><%=comment%></textarea></td>
</tr>
</table>
<br><br>
<table align=center>
<tr>
<td><input type=submit value='수정하기'></input></td>
<td><td><input type=button onClick=location.href='adm_delete.jsp?resv_date=<%=resv_date%>&room=<%=room%>'  value = '삭제'></input></td>
<td><input type=button onClick='goBack();' value = '뒤로가기'> </input></td>
</tr>
</table>
<%
rset.close();
stmt.close();
conn.close();
%>