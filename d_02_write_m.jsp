<!- 실제 글 추가, 수정하는 파일; 웹에 출력되는 부분 없음 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>

<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
</head>
<body>

<%
try{
	request.setCharacterEncoding("UTF-8");
	//d_02.jsp파일에서 파라미터 받아오기
	String name = request.getParameter("name");
	String resv_date = request.getParameter("resv_date");
	String room = request.getParameter("room");
	String addr = request.getParameter("addr");
	String telnum = request.getParameter("telnum");
	String telnum = request.getParameter("telnum");
	String in_name = request.getParameter("in_name");
	String comment = request.getParameter("comment");
	String write_date = request.getParameter("write_date");
	String processing = request.getParameter("processing");


	//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
	
	//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");

	//예약 등록
		Statement stmt = conn.createStatement();
		
		String sql = "insert into resort(name, resv_date, room, addr, telnum, in_name,";
		sql += "comment, write_date, processing) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		
		pstmt.setString(1, name);
		pstmt.setString(2, resv_date);
		pstmt.setString(3, room);
		pstmt.setString(4, addr); 
		pstmt.setString(5, telnum); 
		pstmt.setString(6, in_name); 
		pstmt.setString(7, comment); 
		pstmt.setString(8, write_date); 
		pstmt.setString(9, processing); 
		
		
		pstmt.executeUpdate();
		pstmt.close();
	
%>

<%
	
	conn.close();
	out.println("<script type=\"text/javascript\"> location.href=\"d_01_m.jsp\"; </script>");
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
}
%>

</body>
</html>